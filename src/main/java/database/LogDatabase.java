package database;
import code.*;

import java.util.LinkedList;

/**
 * @author Lucas
 */
public class LogDatabase {

    private LinkedList<Log> list;
    private static LogDatabase instance = null;
    public static LogDatabase getInstance(){
        if(instance == null){
            instance = new LogDatabase();
        }
        return instance;
    }

    private LogDatabase(){
        list = new LinkedList<>();
    }

    /**
     * @param log Add a log to the database of the type Log.
     */
    public void add(Log log){
        list.add(log);
    }

    public Object[] findLog(final String searchTerm){
        return list.stream().
                filter(c -> c.getCustomerID().equals(searchTerm)
                        || c.getMerchantID().equals(searchTerm))
                .distinct().toArray();
    }

}
