package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import code.*;
import database.LogDatabase;

import java.io.Serializable;
import java.util.Optional;

/**
 * @author Erik
 */
@Path("/getLog/{searchTerm}")
public class GetLogsEndpoint {
    LogDatabase db = LogDatabase.getInstance();

    /**
     * @param searchTerm Can be either a merchant CPR or a customer CPR.
     * @return Returns all the logs found in local database involving the merchant or customer
     */
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("searchTerm") String searchTerm) {
        return Response.status(Response.Status.OK).entity(db.findLog(searchTerm)).build();
    }
}

@XmlRootElement
class AddCustomerBody implements Serializable {
    @XmlElement String cpr;
    @XmlElement String name;
}
