package rest;

import code.Log;
import database.LogDatabase;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Mussab
 */
@Path("/addLogs/")
public class AddLogsEndpoint {
    LogDatabase database = LogDatabase.getInstance();

    /**
     * @param body The body of the received JSON to be parsed as a new log.
     * @return Responds to sender. OK for succesful addition, otherwise a BAD_REQUEST error.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLogs(AddBody body){
        try {
            Log log = new Log(body.Token, body.customer, body.merchant, body.money);
            database.add(log);
            return Response.status(Response.Status.OK).build();
        }
        catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }


}

@XmlRootElement
class AddBody implements Serializable {
    @XmlElement
    String Token;
    @XmlElement
    String customer;
    @XmlElement
    String merchant;
    @XmlElement
    float money;
}