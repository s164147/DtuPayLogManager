package code;


/**
 * @author Frederik
 */
public class Log {
    private String token;
    private String customer;
    private String merchant;
    private float money;

    /**
     * @param token authentication token saved as a string.
     * @param customerID CPR of customer involved in transaction.
     * @param merchantID CPR of merchant involved in transaction.
     * @param money Amount of money involved in transaction from customer to merchant.
     */
    public Log(String token, String customerID, String merchantID, float money){
        this.token = token;
        this.merchant = merchantID;
        this.customer = customerID;
        this.money = money;
    }

    /**
     * @return Returns the token as a string.
     */
    public String getToken(){return token;}

    /**
     * @return Returns the customer CPR.
     */
    public String getCustomerID(){
        return customer;
    }

    /**
     * @return Returns merchant CPR.
     */
    public String getMerchantID(){
        return merchant;
    }

    /**
     * @return Returns money involved in transaction.
     */
    public float getMoney(){
        return money;
    }

}
