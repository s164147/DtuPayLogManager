package code;

import rabbitMQServer.RabbitMQConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Erik
 */
public class Main {
    RabbitMQConsumer consumer;

    {
        try {
            consumer = new RabbitMQConsumer();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void main(String args[]) throws IOException {
        consumer.consume();
    }
}
