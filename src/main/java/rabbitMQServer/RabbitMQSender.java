package rabbitMQServer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Burhan
 */
public class RabbitMQSender {
    ConnectionFactory factory = new ConnectionFactory();
    public RabbitMQSender() throws IOException, TimeoutException {
    }

    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    private void consume() throws IOException {
        channel.queueDeclare("Hello world",false,false,false,null);
        String message = "message.";
        channel.basicPublish("","hello world",false,null,message.getBytes());

    }

}
