package rabbitMQServer;

import code.Log;
import com.google.gson.Gson;
import com.rabbitmq.client.*;
import database.LogDatabase;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Burhan
 */
public class RabbitMQConsumer {
    ConnectionFactory factory = new ConnectionFactory();
    public RabbitMQConsumer() throws IOException, TimeoutException {
    }
    LogDatabase lgb = LogDatabase.getInstance();
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    public void consume() throws IOException {
        channel.queueDeclare("Hello world",false,false,false,null);
        channel.basicConsume("Hello world", false, (consumerTag, message) -> {
            String msg = new String(message.getBody(),"UTF-8");
            lgb.add(new Gson().fromJson(msg, Log.class));
            System.out.println(msg);
        }, consumerTag -> {

                }
        );

    }

}
