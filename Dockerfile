FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/LogManager-1.0-SNAPSHOT-thorntail.jar /usr/src
CMD java -Xmx64m \
-Djava.net.preferIPv4Stack=true \
-Djava.net.preferIPv4Addresses=true \
-jar LogManager-1.0-SNAPSHOT-thorntail.jar